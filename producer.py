import pika

connection = pika.BlockingConnection(pika.ConnectionParameters("localhost"))
channel = connection.channel()

channel.queue_declare(queue="prices_fshot")

channel.basic_publish(
    exchange="", routing_key="prices_fshot", body='teste'
)

connection.close()

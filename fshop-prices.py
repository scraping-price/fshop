from selenium import webdriver
from bs4 import BeautifulSoup
import re
import time
from os.path import dirname, realpath, sep, isdir
from os import makedirs, listdir
import json

path = dirname(realpath(__file__))
folder = path+sep+"products"
files = listdir(folder)


def loadFiles(files):

    def loadPrice(linkFixo):
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--incognito')
        driver = webdriver.Chrome(options=options)
        driver.get(linkFixo)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        driver.close()
        element = soup.find("div", class_='main-price')
        text = element.getText()
        text = text.replace("\xa0", "").replace("R$", " R$").replace(
            "|", "").replace("\n", " ").replace("  ", " ")

        pattners = r'R\$\d{1,3}.(?:\d{1,3})?,\d{1,3}'
        values = list(set(re.findall(pattners, text.strip())))

        prices = []
        for i in range(len(values)):
            prices.append(float(values[i].replace(
                'R$', '').replace('.', '').replace(',', '.')))
        prices.sort()

        element = soup.find(id="auto_title_skeleton_box_empty")
        numero = int(time.time())

        path = dirname(realpath(__file__))
        folder = path+sep+"prices"
        if not isdir(folder):
            makedirs(folder)
        filename = folder+sep+str(numero)+".json"
        f = open(filename, "w", encoding="utf-8")
        obj = {}
        obj['link'] = linkFixo
        obj['description'] = element.get_text()


        if len(prices) == 5:
            cont = 1
            for price in prices:
                if cont == 1:
                    obj['price1'] = str(price)+" "+str(int(prices[3]/prices[0]))+"X"
                elif cont == 2:
                    obj['price2'] = str(price)+" pix"
                elif cont == 3:
                    obj['price3'] = str(price)+" boleto, 1x cartão"
                elif cont == 4:
                    obj['price4'] = str(price)+" total a prazo"
                elif cont == 5:
                    obj['price5'] = str(price)+" preço antigo"
                cont=cont+1
                
        if len(prices) == 4:
            cont = 1
            for price in prices:
                if cont == 1:
                    obj['price1'] = str(price)+" "+str(int(prices[3]/prices[0]))+"X"
                elif cont == 2:
                    obj['price2'] = str(price)+" pix, boleto, 1x cartão"
                elif cont == 3:
                    obj['price3'] = str(price)+" a vista"
                elif cont == 4:
                    obj['price4'] = str(price)+" preço total a prazo"
                cont=cont+1
        
        if len(prices) == 1:
            obj['price1'] = str(prices[0])
            
                
        json_string = json.dumps(obj, ensure_ascii=False, indent=2)
        f.writelines(json_string)
        f.close()

    for file in files:
        with open(folder+sep+file, "r") as f:
            linhas = f.readlines()
            for link in linhas:
                time.sleep(1)
                loadPrice(link.replace("\n", ""))


loadFiles(files)

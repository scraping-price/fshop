from selenium import webdriver
from bs4 import BeautifulSoup
import re
import time
from os.path import dirname, realpath, sep, isdir
from os import makedirs

dominio = 'https://www.fastshop.com.br'


def getLinkProduct(linkFixo, count):
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_argument('--incognito')
    driver = webdriver.Chrome(options=options)
    
    driver.get(linkFixo)
    SCROLL_PAUSE_TIME = 0.4

    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

    soup = BeautifulSoup(driver.page_source, 'html.parser')
    driver.close()

    def loadLink(soup):
        path = dirname(realpath(__file__))
        folder = path+sep+"products"
        if not isdir(folder):
            makedirs(folder)
        filename = folder+sep+"fshop-links-products-"+str(count)+".txt"
        f = open(filename, "w")
        links = soup.find_all('a', href=re.compile('/p/.*'))
        lines = []
        for link in links:
            line = ''
            if dominio in link['href']:
                line = link['href']
            else:
                line = dominio+link['href']
            lines.append(line)
        f.writelines([string + '\n' for string in lines])
        f.close()

    loadLink(soup)


path = dirname(realpath(__file__))
folder = path+sep+"home"

with open(folder+sep+"fshop-links-categorias.txt", "r") as f:
    linhas = f.readlines()
    count = 1
    for link in linhas:
        getLinkProduct(link.replace("\n", ""), count)
        count = count + 1


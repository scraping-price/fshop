from selenium import webdriver
from bs4 import BeautifulSoup
import re
import time
from os.path import dirname, realpath, sep, isdir
from os import makedirs

linkFixo = 'https://www.fastshop.com.br/web'
dominio = 'https://www.fastshop.com.br'
options = webdriver.ChromeOptions()
options.add_argument('--headless')
options.add_argument('--incognito')
driver = webdriver.Chrome(options=options)

def getLinkHome(linkFixo):
    driver.get(linkFixo)

    SCROLL_PAUSE_TIME = 0.5

    last_height = driver.execute_script("return document.body.scrollHeight")
    while True:
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(SCROLL_PAUSE_TIME)
        new_height = driver.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height

    soup = BeautifulSoup(driver.page_source, 'html.parser')
    return soup


def loadLink(soup):
    path = dirname(realpath(__file__))
    folder = path+sep+"home"
    if not isdir(folder):
        makedirs(folder)

    f = open(folder+sep+"fshop-links-categorias.txt", "w")

    links = soup.find_all('a', href=re.compile('/c/.*'))
    lines = []
    for link in links:
        line = ''
        if dominio in link['href']:
            line = link['href']
        else:
            line = dominio+"/web"+link['href']
        lines.append(line)
        
    f.writelines([string + '\n' for string in lines])
    f.close()


loadLink(getLinkHome(linkFixo))

driver.close()